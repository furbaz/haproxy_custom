# Custom Router for OpenShift
Follwing these steps you should be able to deploy this custom Router to an OpenShift 1.3 cluster.
The provided config template allows to route tcp traffic based on sni with tls termination at the edge

## Open Ports:
The custom router listens by default on port 5050, this can be changed in the router_dc.yml file. 
To get the router working we need to open this port inside the vm and also on infrastructure level (e.g. AWS)
### On AWS
edit the security group which is assigned to the cluster node, add the port 5050 to the inbound rules
### inside the VM where the router-tcp pod is running
This depends a bit on the default security policy of the IaaS provider. On a custom OpenStack Instance this might not be necessary. 

	sudo iptables -A OS_FIREWALL_ALLOW -p tcp --dport 5050 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

## Configmap with the haproxy template
The haproxy config template gets passed to the pod via a configmap. To create the configmap use this command:
First copy the haproxy-config template to the vm. We need to run the oc commands from inside the vm, because we 
need to have admin rights. 

	oc create configmap customrouter --from-file=haproxy-config-1.3.template

## Create a wildcard TLS key/crt for your domain adress
There are two scripts, one for creating the CA and one for creating the wildcard crt which is signed by our self created CA. First lets create the CA:

	./gen_testca.sh
It worked when a new folder with the name testca is created. There should be some files and folders inside. 
New lets create the wildcard cert for our domain:

	./gen_wildcard_certkey.sh

First it the script asks for the full domain name, here enter something like this:

	*.manual.cr.cloudcomplab.ch

This creates a cert which is valid for all subdomains of the entered one
The second input is the domain name itself, so for example:

	cloudcomplab.ch

## Secret for the wildcard TLS key/crt 
The wildcard cert is passed to the router by a secret.
use this command to create the secret out of it:

	oc create secret tlc router-certs-wildcard --cert=path/to/tls.cert --key=path/to/tls.key

## Router deployment
Finally we can create the custom router deployment. 
First we create the service for it:

	oc create -f router_dc_svc.yml
Then we can create the deployment:
	
	oc create -f router_dc.yml

## Test application
For testing the custom router we use two rabbitmq instances. They can be created with the provided templates inside the rabbitmq folder. 

	oc create -f rabbit-svc_1.yaml
	oc create -f rabbit-pod_1.yaml
The same commands again for the second instance, just replace the name of the template.

## OpenShift routes
To access the created rabbitmq service instances we need to create a route to it.
This can be done via the OpenShift gui. The only thing that needs to be set it the tls termination at the edge.
The certs are not needed because we use the wildcard certs.

## Test the custom router
To test if the connection to the rabbitmq instance is working, we can use the test_tls_conn_os3_komub.py script. 
Before you can run it, change the hostname parameter to the endpoint which was created by the route in OpenShift. 
The server_hostname parameter is used for SNI routing, this has also to be set to the endpoint of the route. 
Finally we can run it, it should print out that it was able to publish a hello message.

	python test_tls_conn_os3_kombu.py
