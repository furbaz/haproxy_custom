mkdir testca
mkdir testca/certs testca/private
chmod 700 testca/private
echo 01 > testca/serial
touch testca/index.txt
cp openssl.cnf testca/openssl.cnf

cd testca && openssl req -x509 -config openssl.cnf -newkey rsa:2048 -days 365 -out cacert.pem -outform PEM -subj /CN=MyTestCA/ -nodes && openssl x509 -in cacert.pem -out cacert.cer -outform DER
