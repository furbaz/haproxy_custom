echo "Please enter the full domain name: "
read full_domain

echo "Please enter the domain name: "
read domain_name


mkdir $full_domain
cd $full_domain && openssl genrsa -out key.pem 2048 && openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=$full_domain/O=$domain_name/ -nodes
cd ../testca && openssl ca -config openssl.cnf -in ../$full_domain/req.pem -out ../$full_domain/cert.pem -notext -batch -extensions server_ca_extensions
