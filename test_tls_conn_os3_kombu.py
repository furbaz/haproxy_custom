import ssl
import os
from kombu import Connection, Exchange, Queue, Consumer
import datetime

try:
  conn_opts = {
      'hostname': "rabbit1-turtlebot.apps.ops3cr.cloudcomplab.ch",
      'port': 5050,
      'userid': "rapyuta_robotics",
      'password': "ECRP_KTI_project",
#      'virtual_host': '/',
#      'heartbeat': 15.0,
      'ssl': dict(        
#        ssl_version=ssl.PROTOCOL_TLSv1,
        context={'cafile':'testca/cacert.pem'},        
        server_hostname="rabbit1-turtlebot.apps.ops3cr.cloudcomplab.ch"),
#      'login_method':  'EXTERNAL',
  }
  # Lets setup a connection sans confirmations for high frequency
  # data
  with Connection(**conn_opts) as conn:
    conn.ensure_connection(max_retries=3)
    print("Connection established to: %s" % conn.info)
    simple_queue = conn.SimpleQueue('simple_queue')
    message = 'helloword, sent at %s' % datetime.datetime.today()
    simple_queue.put(message)
    print('Sent: %s' % message)
    simple_queue.close()
except KeyboardInterrupt:
  loginfo("KeyboardInterrupt received")
